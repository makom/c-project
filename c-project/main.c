#include <stdio.h>
#include <assert.h>
#include <stdbool.h>

typedef struct _node {
	int data;
	struct node* p_next;
} node;

void initialize(node* p_node, int data[], size_t DATA_COUNT);
void initialize_node(node* p_node, int data);
void print(node* p_node);
void add_to_end(node* p_node, int data);
node* add_to_beginning(node* p_node, int data);
void(*callback_func)(node*);
void for_each(node* p_node, void(*callback_func)(node*));
bool(*compare_func)(node*);
node* find_if(node* p_node, bool(*compare_func)(node*));
node* delete_if(node* p_node, bool(*compare_func)(node*));
void reverse(node** p_node);
bool is_circular(node* p_node);
void make_circular(node* p_node);
void make_well_defined(node* p_node);
void print_is_circular(node* p_node);
node* finde_middle(node* p_node);

typedef struct _stack_data_item {
	int data;
} stack_data_item;

typedef struct _stack_node {
	struct stack_data_item* p_data;
	struct stack_node* p_next;
} stack_node;

void initialize(stack_node* p_stack, stack_data_item data[], size_t DATA_COUNT);
void push(stack_node* p_stack, stack_data_item* p_item);
stack_data_item* pop(stack_node* p_stack);
stack_data_item* peek(stack_node* p_stack);

bool compare(node* p_node) {
	if (p_node->data == 55) {
		return 1;
	}
	return 0;
}

int main() {
	const size_t DATA_COUNT = 5;
	int data[] = { 66, 33, 22, 55, 11 };
	node* p_node = calloc(sizeof(node));
	assert(p_node);
	initialize(p_node, data, DATA_COUNT);
	add_to_end(p_node, 99);
	p_node = add_to_beginning(p_node, 44);
	printf("printing each data:\n");
	for_each(p_node, &print);

	node* p_found = find_if(p_node, &compare);
	assert(p_found);
	printf("print found node:\n");
	print(p_found);

	node* p_cleaned = delete_if(p_node, &compare);
	printf("cleaned list from 55:\n");
	for_each(p_cleaned, &print);

	reverse(&p_node);
	printf("print reversed list:\n");
	for_each(p_node, &print);

	print_is_circular(p_node);

	printf("make the list cicular:\n");
	make_circular(p_node);
	print_is_circular(p_node);

	printf("make list well-defined:\n");
	make_well_defined(p_node);
	print_is_circular(p_node);

	printf("print list:\n");
	for_each(p_node, &print);
	printf("finde middle node:\n");
	node* p_middle = finde_middle(p_node);
	print(p_middle);

	return 0;
}

void initialize(node* p_node, int data[], size_t DATA_COUNT) {
	assert(p_node);
	for (size_t i = 0; i < DATA_COUNT; ++i) {
		p_node->data = data[i];
		if (i < (DATA_COUNT - 1)) {
			p_node->p_next = calloc(sizeof(node));
			assert(p_node->p_next);
			p_node = p_node->p_next;
		}
	}
}

void initialize_node(node* p_node, int data) {
	assert(p_node);
	p_node->data = data;
}

void print(node* p_node) {
	printf("value is %d\n", p_node->data);
}

void add_to_end(node* p_node, int data) {
	assert(p_node);
	while (p_node->p_next) {
		p_node = p_node->p_next;
	}
	p_node->p_next = calloc(sizeof(node));
	assert(p_node->p_next);
	initialize_node(p_node->p_next, data);
}

node* add_to_beginning(node* p_node, int data) {
	assert(p_node);
	node* p_begin = calloc(sizeof(node));
	assert(p_begin);
	p_begin->data = data;
	p_begin->p_next = p_node;
	return p_begin;
}

void for_each(node* p_node, void(*compare_func)(node*)) {
	assert(p_node);
	do {
		compare_func(p_node);
		p_node = p_node->p_next;
	} while (p_node);
}

node* find_if(node* p_node, bool(*compare_func)(node*)) {
	assert(p_node);
	do {
		if (compare_func(p_node)) {
			return p_node;
		}
		p_node = p_node->p_next;
	} while (p_node);
	return NULL;
}

node* delete_if(node* p_node, bool(*compare_func)(node*)) {
	assert(p_node);
	node* p_result = NULL;
	do {
		if (!compare_func(p_node)) {
			int data = p_node->data;
			if (!p_result) {
				p_result = calloc(sizeof(node));
				assert(p_result);
				initialize_node(p_result, data);
			}
			else {
				add_to_end(p_result, data);
			}
		}
		p_node = p_node->p_next;
	} while (p_node);
	return p_result;
}

void reverse(node** p_node) {
	assert(*p_node);
	node* p_prev = NULL;
	node* p_current = *p_node;
	node* p_next = NULL;
	while (p_current) {
		p_next = p_current->p_next;
		p_current->p_next = p_prev;
		p_prev = p_current;
		p_current = p_next;
	}
	*p_node = p_prev;
}

bool is_circular(node * p_node) {
	node* const p_head = p_node;
	while (p_node) {
		if (p_node->p_next == p_head)
			return true;
		p_node = p_node->p_next;
	}
	return false;
}

void make_circular(node* p_node) {
	node* const p_head = p_node;
	while (p_node->p_next) {
		p_node = p_node->p_next;
	}
	p_node->p_next = p_head;
}

void make_well_defined(node* p_node) {
	node* const p_head = p_node;
	while (p_node->p_next != p_head) {
		p_node = p_node->p_next;
	}
	p_node->p_next = NULL;
}

void print_is_circular(node* p_node) {
	if (is_circular(p_node))
		printf("list is circular.\n");
	else
		printf("list is not circular.\n");
}

node* finde_middle(node* p_node) {
	assert(p_node);
	node* p_fast = p_node;
	node* p_slow = p_node;
	while (p_fast && p_fast->p_next) {
		node* p_next = p_fast->p_next;
		p_fast = p_next->p_next;
		p_slow = p_slow->p_next;
	}
	return p_slow;
}

void initialize(stack_node* p_stack, stack_data_item data[], size_t DATA_COUNT) {
	assert(p_stack);
	for (size_t i = 0; i < DATA_COUNT; ++i) {
		p_stack->p_data = &(data[i]);
		if (i < (DATA_COUNT - 1)) {
			p_stack->p_next = calloc(sizeof(stack_node));
			assert(p_stack->p_next);
			p_stack = p_stack->p_next;
		}
	}
}

void initialized_stack_node(stack_node* p_node, stack_data_item* p_item) {
	assert(p_node);
	p_node->p_data = p_item;
}

void push(stack_node* p_stack, stack_data_item* p_item) {
	assert(p_stack);
	while (p_stack->p_next) {
		p_stack = p_stack->p_next;
	}
	p_stack->p_next = calloc(sizeof(stack_node));
	initialize_stack_node(p_stack->p_next, p_item);
}

stack_data_item* pop(stack_node* p_stack) {
	assert(p_stack);
	node* p_prev = p_stack;
	while (p_stack->p_next) {

	}

	node* p_last = p_stack;
}

stack_data_item* peek(stack_node* p_stack) {

}
